#!/bin/sh

set -e

space () {
  printf "\n\n"
}

if [ "$WAIT" -gt 0 ]; then
  echo "Waiting $WAIT seconds… ($(date))"
  sleep "$WAIT"
fi

echo "Staring observatory…"

if [ "$#" -gt 0 ]; then
  RES=$(httpobs-local-scan --format report $*)
else
  RES=$(httpobs-local-scan --format report "$HOST")
fi

# parse score from result
SCORE=$(echo "$RES" | head -n1 | sed 's/[^0-9]*//g')

space
echo "$RES"
space
echo "Score is $SCORE/100 (required: ≥ $MIN_SCORE/100)."

# test is failing
if [ "$SCORE" -lt "$MIN_SCORE" ]; then
  exit 1
fi

exit 0
